/*验证%d和%p格式区别*/

#include <stdio.h>

int main() {
	int num = 666;
	printf("%d\n", num);
	printf("%p\n", num);/*针对地址打印，32位环境下打印8位16进制数
						64位环境下打印16位16进制数，位数不够左边补0*/

	printf("%08X\n", num);//和32位下%p打印结果一致
	printf("%016X\n", num);//和64位下%p打印结果一致
}

